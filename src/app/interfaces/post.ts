export interface Post {
    userId: number,
    id: number,
    title: string,
    body: string,
    author: {
      name: string;
      username: string;
    }
    comments: [{
      postId: number,
      id: number,
      name: string,
      email: string,
      body: string
    }],
  }
