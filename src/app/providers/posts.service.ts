import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  
  public UrlAPI:string = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient) {}

  public getAllPosts(id:number):any{
    return this.http.get(this.UrlAPI)
  }
  public getPostById(id:number):any{
    return new Promise((response)=>{
      this.http.get(this.UrlAPI + id).subscribe((post)=>{
        response(post)
      })
    })
  }
  public createPost(post:any){
    return new Promise((response)=>{
      this.http.post(this.UrlAPI, JSON.stringify(post)).subscribe((res)=>{
        response(true)
      })
    }) 
      
  }
  public updatePost(post:any,id:number){
    return new Promise((response)=>{
      this.http.put(this.UrlAPI + id, JSON.stringify(post)).subscribe((res)=>{
        response(true)
      })
    }) 
      
  }
  public deletePost(id:number){
    return new Promise((response)=>{
      this.http.delete(this.UrlAPI + id).subscribe((res)=>{
        response(true)
      })
    }) 
      
  }
  public getComentaries(id:number){
    return new Promise((response)=>{
      this.http.get(this.UrlAPI + id + "/comments").subscribe((comments)=>{
        response(comments)
      })
    })
  }
}
