import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Photo } from '../interfaces/photo';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  public UrlAPI:string = "https://jsonplaceholder.typicode.com/";

  constructor(private http:HttpClient) {}

  public getAllPhotos(id):any{
    return new Promise((response)=>{
      this.http.get(this.UrlAPI + "albums/" + id + "/photos" ).subscribe((Photos:any)=>{
        response(Photos)
      })
    })
  }
  public getPhotoById(id:number):any{
    return new Promise((response)=>{
      this.http.get(this.UrlAPI + "photos/" + id).subscribe((Photo:Photo)=>{
        response(Photo)
      })
    })
  }
}
