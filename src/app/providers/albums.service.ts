import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {
  public UrlAPI:string = "https://jsonplaceholder.typicode.com/users/1/albums";

  constructor(private http:HttpClient) { }
  
  public getAllAlbums(){
    return new Promise((response)=>{
      this.http.get(this.UrlAPI).subscribe((albums:any)=>{
        response(albums)
      })
    })
  }
}
