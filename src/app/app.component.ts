import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UsersService } from './providers/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  public appPages = [
    {
      title: 'Home',
      url: '/app/home',
      icon: 'home'
    },
    {
      title: 'Friends',
      url: '/app/friends',
      icon: 'people'
    },
    {
      title: 'Albums',
      url: '/app/albums',
      icon: 'albums'
    },
    {
      title: 'Tasks',
      url: '/app/tasks',
      icon: 'pricetag'
    }
  ];
  public dark = false;

  constructor(public userService:UsersService) { }

 ngOnInit() { }
}
