import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicModule } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModulesModule } from './modules/material-modules/material-modules.module';
import { UsersService } from './providers/users.service';
import { PostsService } from './providers/posts.service';
import { TasksService } from './providers/tasks.service';
import { AlbumsService } from './providers/albums.service';
import { PhotosService } from './providers/photos.service';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    }),
    BrowserAnimationsModule,
    MaterialModulesModule,
  ],
  declarations: [AppComponent],
  providers: [InAppBrowser,UsersService,PostsService,TasksService,AlbumsService,PhotosService],
  bootstrap: [AppComponent]
})
export class AppModule {}
