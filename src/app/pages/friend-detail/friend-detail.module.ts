import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FriendDetailPageRoutingModule } from './friend-detail-routing.module';

import { FriendDetailPage } from './friend-detail.page';
import { MaterialModulesModule } from '../../modules/material-modules/material-modules.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FriendDetailPageRoutingModule,
    MaterialModulesModule
  ],
  declarations: [FriendDetailPage]
})
export class FriendDetailPageModule {}
