import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../providers/users.service';
import { User } from '../../interfaces/user';
import {FormControl, Validators} from '@angular/forms';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  public profilePhoto:any;
  public data:User = {
    id: -1,
    name: "",
    username: "",
    email: "",
    address: {
        street: "",
        suite: "",
        city: "",
        zipcode: "",
        geo: {
        lat: "",
        lng: ""
        }
    },
    phone: "",
    website: "",
    company: {
        name: "",
        catchPhrase: "",
        bs: ""
    }
  };
  public email = new FormControl('', [Validators.required, Validators.email]);
  public appPages = [
    {
      title: 'Home',
      url: '/app/home',
      icon: 'home'
    },
    {
      title: 'Friends',
      url: '/app/friends',
      icon: 'people'
    },
    {
      title: 'Albums',
      url: '/app/albums',
      icon: 'albums'
    },
    {
      title: 'Tasks',
      url: '/app/tasks',
      icon: 'pricetag'
    }
  ];
  
  constructor(public userService:UsersService,
              public loadingController: LoadingController,
              public toastController: ToastController) { 
    this.profilePhoto = "../../../assets/img/profile.png";
  }

  async ngOnInit() { 
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present().then(()=>{
      this.userService.getUser().then((user:User)=>{
        this.data = user;
        loading.dismiss()
      })
    })
  }
  public async saveData(){
    if(this.data != this.userService.user){
      const loading = await this.loadingController.create({
        message: 'Saving data...',
      });
      await loading.present().then(()=>{
        this.userService.updateUser(this.data,1).then(()=>{
          loading.dismiss();
          this.presenToast("All the data is saved!")
        })
      })
    }
  }
  public getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
  public changeAvatar($event){
    let fileReader = new FileReader();
    fileReader.readAsDataURL($event.target.files[0]);
    fileReader.onload = () => {
        this.profilePhoto = fileReader.result;
    }   

  }
  public async presenToast(message, duration?){
    let toast = await this.toastController.create({
      message: message,
      duration: duration || 5000,
    })
    return await toast.present()
  }

}
