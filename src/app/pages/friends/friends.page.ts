import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../providers/users.service';
import { User } from '../../interfaces/user';
import { ModalController } from '@ionic/angular';
import { FriendDetailPage } from '../friend-detail/friend-detail.page';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.page.html',
  styleUrls: ['./friends.page.scss'],
})
export class FriendsPage implements OnInit {
  public users:Array<User> = [];
  public counts = [1,2,3,4,5,6,7];
  constructor(public usersService:UsersService,
      public modalController: ModalController,) { }

  ngOnInit() {
    this.usersService.getAllUsers().then((users)=>{
      this.users = users;
    })
  }
  public async presentModal(user) {
    const modal = await this.modalController.create({
      component: FriendDetailPage,
      cssClass: 'my-custom-class',
      componentProps: {
        user: user
      },
    });
    return await modal.present();
  }

}
