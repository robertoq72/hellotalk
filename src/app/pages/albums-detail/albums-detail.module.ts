import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlbumsDetailPageRoutingModule } from './albums-detail-routing.module';

import { AlbumsDetailPage } from './albums-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlbumsDetailPageRoutingModule
  ],
  declarations: [AlbumsDetailPage]
})
export class AlbumsDetailPageModule {}
