import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs-page';
import { TabsPageRoutingModule } from './tabs-page-routing.module';

import { HomePageModule } from '../home/home.module';
import { FriendsPageModule } from '../friends/friends.module';
import { AlbumsPageModule } from '../albums/albums.module';
import { AlbumsDetailPageModule } from '../albums-detail/albums-detail.module';
import { TasksPageModule } from '../tasks/tasks.module';

@NgModule({
  imports: [
    CommonModule,
    TabsPageRoutingModule,
    IonicModule,
    HomePageModule,
    FriendsPageModule,
    AlbumsPageModule,
    AlbumsDetailPageModule,
    TasksPageModule
  ],
  declarations: [
    TabsPage,
  ]
})
export class TabsModule { }
