import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs-page';
import { HomePage } from '../home/home.page';


const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            component: HomePage,
          }
        ]
      },
      {
        path: 'friends',
        children: [
          {
            path: '',
            loadChildren: () => import('../friends/friends.module').then(m => m.FriendsPageModule)
          },
        ]
      },
      {
        path: 'albums',
        children: [
          {
            path: '',
            loadChildren: () => import('../albums/albums.module').then(m => m.AlbumsPageModule)
          },
          {
            path: 'album/:albumId',
            loadChildren: () => import('../albums-detail/albums-detail.module').then(m => m.AlbumsDetailPageModule)
          },
        ]
      },
      {
        path: 'tasks',
        children: [
          {
            path: '',
            loadChildren: () => import('../tasks/tasks.module').then(m => m.TasksPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/app/home',
        pathMatch: 'full'
      },
      {
        path: '**',
        redirectTo: '/app/home',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }

