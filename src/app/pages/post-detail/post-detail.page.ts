import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../interfaces/post';
import { PostsService } from '../../providers/posts.service';
import { LoadingController, ModalController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.page.html',
  styleUrls: ['./post-detail.page.scss'],
})
export class PostDetailPage implements OnInit {
  @Input() post: Post;
  @Input() create : boolean;
  constructor(public postService:PostsService,
              public loadingController: LoadingController,
              public alertController: AlertController,
              public modalController: ModalController) { }

  ngOnInit() {
  }
  public async saveData(){
    const loading = await this.loadingController.create({
      message: 'Publing post...',
    });
    await loading.present().then(()=>{
      if(this.create == true){
        console.log("creating")
        this.postService.createPost(this.post).then((res)=>{
          loading.dismiss()
          this.modalController.dismiss(this.post)
          console.log(res)
        })
      }else{
        console.log("updating")
        this.postService.updatePost(this.post, this.post.id).then(()=>{
          loading.dismiss()
          this.modalController.dismiss(this.post)
        })

      }
    })
  }
  public deletePost(){
    let buttons = [
      {
        text: "Delete",
        handler: async ()=>{
          const loading = await this.loadingController.create({
            message: 'Deleting post...',
          });
          await loading.present().then(()=>{
            this.postService.deletePost(this.post.id).then(()=>{
              loading.dismiss();
              this.modalController.dismiss(false)
            })
          })
        }
      },
      {
        text: "Cancel"
      }
    ]
    this.presentAlert("Delete Post", this.post.title,this.post.body, buttons)
  }
  public async presentAlert(title,subtitle,body,buttons?) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: subtitle,
      message: body,
      buttons: buttons || ['Close']
    });

    await alert.present();
  }
}
